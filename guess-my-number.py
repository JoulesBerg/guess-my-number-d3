input("Welcome to the Guess my number game, Press enter to continue")

var = input("Please pick a number between 1 and 50, then press enter: ")
try:
    var = int(var)
except:
    print("Your input is not a valid integer, please restart the program")
    exit()

varmax = 50
varmin = 1
legal = var >= varmin and var <= varmax
guesses = 0
maxguessesstr = input("How many guesses would you like me to make?")
try:
    maxguesses = int(maxguessesstr)
except: 
    print("You did not enter a valid integer, please restart the program")
    exit()
guessed = False
storage = []
if legal == True:
    while guessed == False and guesses <= maxguesses-1:
        median = (varmax+varmin)//2
        medianstr = str(median)
        response = input("I think your number might be " + medianstr + ". Is that correct? y/n: ")
        if response == "y" or response == "Y" or response == "yes" or response == "Yes":
            guessed = True
        else:
            response = input("Is your number greater than "+medianstr+"? y/n: ")
            if response == "y":
                print("Thank you for your feedback, I will try again")
                storage.append(median)
                varmin = median +1
            elif response == "n":
                print("Thank you for your feedback, I will try again")
                storage.append(median)
                varmax = median -1
            else: 
                print("Invalid answer, lets go back to where we were.")
        guesses = guesses + 1
else:
    print("The number you entered is not between 1 and 50, please restart the program and don't try to cheat")
if guessed == True:
    print("You suck at picking numbers, that was easy after I guessed the numbers below")
    for number in storage:
        print(number)
    exit()